package com.shopping.service;

import org.springframework.stereotype.Service;

import com.shopping.model.User;

@Service
public interface UserService {
	
	public User addUser(User user);

	public User login(String email, String password);
}
