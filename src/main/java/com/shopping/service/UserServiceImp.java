package com.shopping.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopping.model.User;
import com.shopping.repository.UserRepository;

@Service
public class UserServiceImp implements UserService {
	
	@Autowired
	UserRepository userRepo;
	
	public User addUser(User user){
		return userRepo.save(user);
	}

	@Override
	public User login(String email, String password) {
		return userRepo.findByEmailAndPassword(email, password);
	}
}
