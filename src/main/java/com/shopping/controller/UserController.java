package com.shopping.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.shopping.dto.UserDto;
import com.shopping.model.User;
import com.shopping.service.UserService;

@Controller
public class UserController {
	
	@Autowired
	UserService userService;
	
	@ResponseBody
	@PostMapping(value = "/registration")
	public UserDto registration(@RequestBody @ModelAttribute User user) {
		userService.addUser(user);
		UserDto uderDto = new UserDto(user.getEmail(), user.getFirstName());
		return uderDto;
	}
	
	@ResponseBody
	@GetMapping(value = "/login")
	public UserDto login(@RequestParam("Email") String email, @RequestParam("Password") String password) {
		
		User user = userService.login(email, password);
		
		if(user != null) {
			return new UserDto(user.getEmail(), user.getFirstName());
		}
		
		return null;
	}
}
