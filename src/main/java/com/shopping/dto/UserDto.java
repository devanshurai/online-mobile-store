package com.shopping.dto;

public class UserDto {
	
	private String email;
	
	private String firstName;
	
	public UserDto() {
	}

	public UserDto(String email, String firstName) {
		super();
		this.email = email;
		this.firstName = firstName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Override
	public String toString() {
		return "UserDto [email=" + email + ", firstName=" + firstName + "]";
	}
}
